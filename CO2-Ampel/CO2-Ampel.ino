#include <LiquidCrystal.h>
#include <SoftwareSerial.h>
#include <MHZ.h> // min. Version >= 1.2.1
#include <Wire.h>
#include <SparkFunCCS811.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <EEPROM.h>

// CO2-Ampel
// mit zwei unterschiedlichen Sensortypen (NDIR und MOX)
// 2020/2021 Daniel Hirscher

// Klassifizierung der Raumluftqualität nach DIN EN 13779 in Stufen
//  (IDA = Indoor Air) in ppm (Parts per Million = Teilchen pro Million):
// IDA-1: < 800 ppm = Hohe Raumluftqualität -> grün
// IDA-2: 800 - 1000 ppm = Mittlere Raumluftqualität -> grün + gelb
// IDA-3: > 1000 - 1400 ppm = Mäßige Raumluftqualität -> gelb
// IDA-4: > 1400 ppm = Niedrige Raumluftqualität -> rot

// tVOC-Einteilung nach Umweltbundesamt in mg/m³, sowie vom CCS811-Sensor
//  gelieferten ppb (Parts per Billion = Teilchen pro Milliarde):
// <1 mg/m³ ca. <150 bis 400 ppb -> unbedenklich
// <1 bis 3 mg/m³ ca. 150 bis 1300 ppb -> auffällig
// >10 mg/m³ ca. über 1500 bis 4000 ppb -> inakzeptabel

#define CCS811_ADDR 0x5A

#define LED_ROT 10
#define LED_GELB 9
#define LED_GRUEN 8

MHZ mhz(11, 12, MHZ14A);
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);
CCS811 ccs(CCS811_ADDR);
Adafruit_BME280 bme;

int ppmMHZ = -1;

// zyklisches Durchschalten weniger wichtiger Werte auf dem Display
long cycleTime = 0;
const long cyclePeriod = 5000L;
int cyclePos = 0;

// gelbe LED blinkt während der Aufheizzeit des MH-Z-Sensors
bool blink = true;

// CCS ppm: gleitender Durchschnitt über die letzten 100 Werte
int ppms[100];
int ppmPos = 0;
int ppmCount = 0;

// für die Formatierte Ausgabe mit einer Dezimalstelle
static char str[20];
static char decimalValue[10];

// Symbol für: zyklisches Durchschalten
byte CYCLE[8] =   { 0b00000,
                    0b00101,
                    0b01011,
                    0b10111,
                    0b10000,
                    0b01001,
                    0b00110,
                    0b00000 };

// Symbol für: tiefgestellte Zwei für CO2
byte ZWEI[8] =    { 0b00000,
                    0b00000,
                    0b01100,
                    0b10010,
                    0b00010,
                    0b00100,
                    0b01000,
                    0b11110 };

// CCS811 Baseline
#define ADDR_BASELINE 16
const unsigned long CCS_CONDITIONING_TIME = 30L * 60L * 1000L;
const unsigned long CCS_BASELINE_TIME = 60L * 60L * 1000L;
unsigned long ccsNextBaselineSaveTime = CCS_BASELINE_TIME;
boolean ccsBaselineAvailable = false;
uint16_t ccsBaseline = 0;
// Datenstruktur für CCS811 Baseline
struct CCSBaseline {
   char identifier[6];
   uint16_t baseline;
};

void setup() {
  // Interne LED ausschalten
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);

  // LEDs der Ampel
  pinMode(LED_ROT, OUTPUT);
  pinMode(LED_GELB, OUTPUT);
  pinMode(LED_GRUEN, OUTPUT);
  setAmpel(true, true, true);

  // serielle Verbindung über USB
  Serial.begin(9600);
  Serial.println("CO2-Ampel (NDIR+MOX)\n");

  // I2C-Bus für CCS811 und BME280
  Wire.begin();

  // LCD inititalisieren
  lcd.begin(16, 2);
  lcd.createChar(1, CYCLE);
  lcd.createChar(2, ZWEI);
  lcd.setCursor(0, 0);
  lcd.print("CO\002-Ampel");

  while (!ccs.begin())
  {
    Serial.println("CCS811 fehlt ...");
    lcd.setCursor(0, 1);
    lcd.print("CCS811 fehlt ...");
    delay(1000);
  }
  // Messung 1x pro Sekunde
  ccs.setDriveMode(1);
  // Baseline ausgeben, falls vorhanden
  CCSBaseline data = { "------", 0 };
  EEPROM.get(ADDR_BASELINE, data);
  if (strncmp("CCS811", data.identifier, 6) == 0)
  {
    ccsBaselineAvailable = true;
    ccsBaseline = data.baseline;
    Serial.print("CCS811-Baseline: ");
    Serial.print(ccsBaseline);
    Serial.println("\n");
  }
  else
  {
    Serial.println("Keine CCS811-Baseline gefunden.\n");
  }

  while(!bme.begin())
  {
    Serial.println("BME280 fehlt ...");
    lcd.setCursor(0, 1);
    lcd.print("BME280 fehlt ...");
    delay(1000);
  }

  setAmpel(false, false, false);
}

void loop() {
  Serial.print(millis() / 1000);
  Serial.print(" s    ");

  if (mhz.isPreHeating()) {
    setAmpel(false, blink, false);
    blink = !blink;
  } else {
    int ppmMHZValue = mhz.readCO2UART();
    if (ppmMHZValue > 0) {
      ppmMHZ = ppmMHZValue;
      updateAmpel(ppmMHZ);
    }
  }

  // BME-Werte zuerst lesen und dann dem CCS811 geben
  float humidity = bme.readHumidity();
  float temperature = bme.readTemperature();
  float pressure = bme.readPressure() / 100.0F;

  int tVOC = -1;
  int ppmAvg = -1;
  if (ccs.dataAvailable())
  {
    ccs.readAlgorithmResults();
    int ppmCCS = ccs.getCO2();
    tVOC = ccs.getTVOC();
    ccs.setEnvironmentalData(humidity, temperature);

    // gleitenden Durchschnitt berechnen
    ppms[ppmPos] = ppmCCS;
    if (ppmCount < 100) ++ppmCount;
    if (ppmPos < 100) ++ppmPos; else ppmPos = 0;
    long ppmTotal = 0;
    for (int i = 0; i < ppmCount; ++i) ppmTotal += ppms[i];
    ppmAvg = ppmTotal / ppmCount;

    if (ccsBaselineAvailable && millis() > CCS_CONDITIONING_TIME)
    {
      ccs.setBaseline(ccsBaseline);
      ccsBaselineAvailable = false;
    }
    if (millis() > ccsNextBaselineSaveTime)
    {
      CCSBaseline data = { "CCS811", ccs.getBaseline() };
      EEPROM.put(ADDR_BASELINE, data);
      ccsNextBaselineSaveTime = millis() + CCS_BASELINE_TIME;
    }
  }

  lcd.setCursor(0, 0);
  Serial.print("ppmMHZ: ");
  if (ppmMHZ > 0 && ppmAvg > 0) {
    snprintf(str, 20, "CO\002 %4d/%4dppm", ppmMHZ, ppmAvg);
  }
  else if (ppmAvg > 0)
  {
    Serial.print("n/a (code=");
    Serial.print(ppmMHZ);
    Serial.print(")   ");
    snprintf(str, 20, "CO\002 ----/%4dppm", ppmAvg);
  }
  else
  {
    snprintf(str, 20, "CO\002 ----/----ppm");
  }
  lcd.print(str);
  Serial.print(str);
  Serial.print("   ");

  lcd.setCursor(0, 1);
  dtostrf(temperature, 2, 1, decimalValue);
  decimalValue[2] = ',';
  snprintf(str, 20, "%s\337C ", decimalValue);
  lcd.print(str);
  Serial.print(str);
  switch (cyclePos) {
    case 0:
      dtostrf(humidity, 2, 1, decimalValue);
      decimalValue[2] = ',';
      snprintf(str, 20, "%s%%   \001", decimalValue);
      break;
    case 1:
      snprintf(str, 20, "%4dhPa \001", round(pressure));
      break;
    case 2:
      snprintf(str, 20, "%4dppb \001", tVOC);
      break;
  }
  if (cycleTime < millis())
  {
    cyclePos = (cyclePos + 1) % 3;
    cycleTime += cyclePeriod;
  }
  lcd.print(str);
  Serial.println(str);

  delay(1000);
}

// nach IDA, wie oben beschrieben
void updateAmpel(int ppm)
{
  if (ppm < 800)
  {
    setAmpel(false, false, true);
  }
  else if (ppm <= 1000)
  {
    setAmpel(false, true, true);
  }
  else if (ppm <= 1400)
  {
    setAmpel(false, true, false);
  }
  else
  {
    setAmpel(true, false, false);
  }
}

void setAmpel(bool rot, bool gelb, bool gruen)
{
  digitalWrite(LED_ROT, rot ? HIGH : LOW);
  digitalWrite(LED_GELB, gelb ? HIGH : LOW);
  digitalWrite(LED_GRUEN, gruen ? HIGH : LOW);
}
