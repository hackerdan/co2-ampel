CO<sub>2</sub>-Ampel
====================

Mit zwei unterschiedlichen Sensoren den CO<sub>2</sub>-Gehalt in der Luft messen.

Anzeige der Werte auf einem LC-Display, sowie der Wert des NDIR-Sensors als LED-Ampel.

Die Ampel zeigt die Werte gemäß der Klassifizierung der Raumluftqualität nach DIN EN 13779 in den dort definierten IDA-Stufen (IDA = Indoor Air) in ppm (parts per million = Teilchen pro Million) an:

Stufe | Wertebereich      | Beschreibung              | Ampelfarbe
------|-------------------|---------------------------|-----------
IDA-1 | < 800 ppm         | Hohe Raumluftqualität     | grün
IDA-2 | 800 - 1000 ppm    | Mittlere Raumluftqualität | grün + gelb
IDA-3 | > 1000 - 1400 ppm | Mäßige Raumluftqualität   | gelb
IDA-4 | > 1400 ppm        | Niedrige Raumluftqualität | rot

## Material

- Arduino Uno
- NDIR-CO<sub>2</sub>-Sensor MH-Z14A
- MOX-CO<sub>2</sub>- und tVOC-Sensor CCS811
- Temeratur- und Feuchtesensor BME280
- 4 Widerstände mit 220 &Omega;
- Potentiometer mit 10 k&Omega;
- Breadboard
- Breadboardkabel
- LC-Display 16x2 Zeichen

## Aufbau

![Verkabelung auf Breadboard](CO2-Ampel_Steckplatine.png)

![Foto vom Aufbau](Breadboardaufbau.jpg)

![Foto vom Gehäuse (vorne)](case-front.jpg)
![Foto vom Gehäuse (hinten)](case-back.jpg)

### Pin-Belegung MH-Z14A

PIN | MH-Z14A | Arduino Uno
----|---------|------------
16  | V-   |  GND
17  | V+   |  5 V
18  | R    |  12
19  | T    |  11

Alle anderen Pins werden nicht angeschlossen.

### Pin-Belegung CCS811

CCS811 | Arduino Uno
-------|------------
VCC    | 3,3 V
GND    | GND
SCL    | A5
SDA    | A4
WAK    | GND

Alle anderen Pins werden nicht angeschlossen.

### Pin-Belegung BME280

BME280                | Arduino Uno
----------------------|------------
VCC                   | 3,3 V
GND                   | GND
SCL                   | A5
SDA                   | A4
CS (falls vorhanden)  | GND
SD0 (falls vorhanden) | 3,3 V

### Pin-Belegung LC-Display

PIN | LC-Display | Arduino Uno
----|------------|-------------------
 1  | Vss        | GND
 2  | Vcc        | 5 V
 3  | Vee        | GND via 10 k&Omega;
 4  | RS         | 7
 5  | R/W        | GND
 6  | EN         | 6
 7  | D0         | nicht angeschlossen
 8  | D1         | nicht angeschlossen
 9  | D2         | nicht angeschlossen
10  | D3         | nicht angeschlossen
11  | D4         | 5
12  | D5         | 4
13  | D6         | 3
14  | D7         | 2
15  |            | 5 V via 220 &Omega;
16  |            | GND

### Pin-Belegung LEDs

LED  | Arduino Uno
-----|------------
rot  | 8
gelb | 9
grün | 10

### Ausgabe auf dem LC-Display

```
1234567890123456
CO2 ----/----ppm
--,-°C --,-%   •
--,-°C ----hPa •
--,-°C ----bbp •
```

CO<sub>2</sub> wird von den beiden Sensoren MH-Z14A und CCS811 angezeigt, Temperatur, Feuchte und Luftdruck vom BME280 und tVOC vom CCS811.

Die rechte Hälfte der Anzeige in der zweiten Zeile wechselt alle 5 Sekunden zyklisch durch.

### Fazit

#### NDIR-Sensor MH-Z14A

Die Werte vom NDIR-Sensor (nichtdispersiver Infrarotsensor) MH-Z14A scheinen sinnvoll zu sein. Sie erhöhen sich, wenn sich mehrere Personen im Raum aufhalten und sinken wieder, wenn eine Zeit lang niemand im Raum ist. Auch das Lüften wirkt sich nach ein paar Minuten auf die Werte aus. Der Sensor ist mit 15&nbsp;€ auf dem globalen Markt recht teuer, hat dafür aber nur eine Aufwärmzeit von 3&nbsp;Minuten.

#### MOX-Sensor CCS811

Die Werte vom MOX-Sensor (Metalloxid) CCS811 erscheinen mir trotz Baseline und Run-In-Phase von 20&nbsp;Minuten unrealistisch. Oft sind sie viel zu hoch und ändern sich kaum, wenn gelüftet wird. Er hat zwar nur 7&nbsp;€ gekostet, aber ich traue weder dem CO<sub>2</sub>-Wert, noch dem tVOC-Wert.

### Referenzen

- [Arduino MH-Z Library auf GitHub](https://github.com/tobiasschuerg/MH-Z-CO2-Sensors)
- [MH-Z14A User's Manual V1.01 vom 24.9.2015](https://www.winsen-sensor.com/d/files/infrared-gas-sensor/mh-z14a_co2-manual-v1_01.pdf)
- [MH-Z14 User's Manual V2.4 vom 1.5.2014, enthält Protokollbeschreibung](https://www.winsen-sensor.com/d/files/MH-Z14.pdf)
- [CCS811 Datasheet](https://cdn.sparkfun.com/assets/learn_tutorials/1/4/3/CCS811_Datasheet-DS000459.pdf)
- [CCS811 Application Note: Baseline Save and Restore](https://www.sciosense.com/wp-content/uploads/2020/01/Application-Note-Baseline-Save-and-Restore-on-CCS811.pdf)
- [CO<sub>2</sub> als Lüftungsindikator](http://raumluft.linux47.webhome.at/natuerliche-mechanische-lueftung/co2-als-lueftungsindikator/)
- [Umweltbundesamt: Gesundheitliche Bewertung von Kohlendioxid in der Innenraumluft](https://www.umweltbundesamt.de/sites/default/files/medien/pdfs/kohlendioxid_2008.pdf)
- [Umweltbundesamt: Richtwerte für die Innenraumluft (TVOC-Wert)](https://www.umweltbundesamt.de/sites/default/files/medien/pdfs/TVOC.pdf)
- [Gesundheit: Beurteilung nach dem TVOC-Konzept](https://www.lgl.bayern.de/gesundheit/arbeitsplatz_umwelt/chemische_umweltfaktoren/beurteilung_tvoc_konzept.htm)